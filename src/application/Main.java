package application;

import javax.swing.SwingUtilities;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * A main class to run the audioplayer GUI and its functions.
 * @author Pakpon Jetapai and Phasin Sarunpornkul
 */

public class Main extends Application{

	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/MusicPlayerUI.fxml"));
			Scene scene = new Scene( root );
			primaryStage.setOnShowing( windowEvent -> {
			});
			primaryStage.setTitle("Audio Player");
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.getIcons().add(new Image("/images/audioplayer.png"));
			primaryStage.show();
			primaryStage.setOnCloseRequest( windowEvent -> {

			});
		} 

		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Main method that runs the application, initialize components and make them visible.
	 * @param args not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}

