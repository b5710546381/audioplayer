package fxml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * A class that input and output playlist files of the program
 * @author Pakpon Jetapai
 *
 */
public class PlaylistIO implements Serializable {
	private ArrayList<File> audioFiles;
	private static ObjectInputStream ois;
	
	/**
	 * Initialize PlaylistIO and its components.
	 */
	public PlaylistIO() {
		audioFiles = new ArrayList<File>();
	}
	
	/**
	 * Get the stored audio file list.
	 * @return the stored audio file list
	 */
	public ArrayList<File> getAudioFiles() { return audioFiles; }
	
	/**
	 * Add a file to the stored audio file list.
	 * @param file - a file to be added to stored audio file list.
	 */
	public void addFile( File file ) { audioFiles.add( file ); }
	
	/**
	 * Read a playlist file chosen by user and return to its output state.
	 * @param file - playlist file to be read
	 * @return a previously output playlist object
	 */
	public static PlaylistIO readPlaylist( File file ) {
		try {
			ois = new ObjectInputStream( new FileInputStream( file ) );
			try {
				return (PlaylistIO) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new PlaylistIO();
	}
	
	/**
	 * Write a playlist object file to a specific directory
	 * @param title - title of the playlist object file
	 * @param directory - a specific directory that the file will be saved to
	 */
	public void writePlaylist( String title, File directory ) {
		File file = new File( directory, title + ".pl4" );
		try {
			FileOutputStream fos = new FileOutputStream( file );
			try {
				ObjectOutputStream oos = new ObjectOutputStream( fos );
				oos.writeObject( this );
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
}
