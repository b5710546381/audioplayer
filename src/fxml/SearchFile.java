package fxml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that searchs for a file with supported extension.
 * @author Phasin Sarunpornkul
 *
 */
public class SearchFile {
	
	private List<File> fileList;

	/**
	 * Initialize SearchFile and its components.
	 */
	public SearchFile(){
		fileList = new ArrayList<File>();
	}

	/**
	 * Searched for files with specific name and directory then add
	 * them to a list of file.
	 * @param name - name of files to be searched for
	 * @param files - directory that will be searched
	 */
	public void search( String name,File files ){
		File[] list = files.listFiles();
		if( list!=null )
			for ( File file : list )
			{
				if ( file.isDirectory() )
				{
					search(name,file);
				}
				else if( file.getName().toLowerCase().contains(name.toLowerCase()) || file.getParent().toLowerCase().contains(name.toLowerCase()) ) {
					if( file.getName().endsWith(".mp3")||file.getName().endsWith(".wav")||file.getName().endsWith(".acc") ){
						fileList.add(file);
					}
				}
			}
	}

	/**
	 * Get the stored list of files that have been searched.
	 * @return searched file list
	 */
	public List<File> getFileList(){
		return this.fileList;
	}
}
