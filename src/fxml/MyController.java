package fxml;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.SwingWorker;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;

/**
 * A controller class for controlling the components in GUI.
 * It also handle playing audio file and delegate PlaylistIO
 * for inputting or outputting playlist files.
 * @author Pakpon Jetapai and Phasin Sarunpornkul
 */
public class MyController implements Initializable {

	private final FileChooser fileChooser = new FileChooser();
	private ObservableList<String> items;
	private ArrayList<File> filelist;
	private ObservableList<String> itemsSearch;
	private ArrayList<File> fileSearch;
	private MediaPlayer mediaplayer;
	private ProgressBar pb;
	private Duration duration;
	private boolean ShuffleOn = false;
	private boolean RepeatOn = false;
	private boolean RepeatAllOn = false;
	private int playingIndex;
	private int playlistCount = 1;
	private double prevVolume;
	private List<ListView<String>> storeListView = new ArrayList<ListView<String>>();
	private List<ObservableList<String>> storeItemsList = new ArrayList<ObservableList<String>>();
	private List<ArrayList<File>> storefileList = new ArrayList<ArrayList<File>>();
	private int currentTabIndex = 0;
	private ListView<String> currentListView;
	private ArrayList<File> currentFileList;
	private ObservableList<String> currentItemList;
	
	@FXML
	AnchorPane pane;
	
	@FXML
	private Pane mainPanel;
	
	@FXML
	private Button browsebtn, playbtn, pausebtn, stopbtn, shufflebtn, repeatbtn, repeatAllbtn, previousbtn, nextbtn, searchbtn;

	@FXML
	private ListView<String> listview;

	@FXML
	private Slider timeSlider = new Slider();

	@FXML
	private Slider volumeSlider = new Slider();

	@FXML
	private Label playTime = new Label();

	@FXML
	private Label playingSong = new Label();

	@FXML
	private Label statusLabel = new Label();

	@FXML
	protected TabPane tabPane = new TabPane() ;

	@FXML
	private Tab firstTab = new Tab( "Playlist"+playlistCount );

	@FXML
	private Tab addTab = new Tab( "+" );

	@FXML
	private Tab searchTab = new Tab( "search" );

	@FXML
	private TextField searchField = new TextField();

	@FXML
	private ListView<String> searchList = new ListView<String>(); 

	@FXML
	private ComboBox<String> driveBox = new ComboBox<String>();
	private ObservableList<String> drive;

	/**
	 * A method that open file chooser up and allow user to select 
	 * multiple supported files and put them in the current playlist.
	 */
	@FXML
	public void openFile(){
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Audio Files (.mp3,.wav,.aac)","*.mp3","*.wav",".*aac", "*.pl4"));
		List<File> files = fileChooser.showOpenMultipleDialog(null);
		System.out.println( "Checking file");
		if (files != null) {
			for(File file : files){
				String actualPath = file.toURI().toASCIIString();
				if( actualPath.endsWith( ".pl4" ) ) {
					PlaylistIO pio = PlaylistIO.readPlaylist( file );
					for( File audioFile: pio.getAudioFiles() ) {
						System.out.println( audioFile.getName() );
						currentItemList.add( audioFile.getName() );
						currentFileList.add( audioFile );
					}
				}
				else {
					currentItemList.add( file.getName() );
					currentFileList.add( file );
				}
			}
		}
	}

	/**
	 * A method that search for the current selected playlist on the tab and
	 * change the current displaying list. 
	 */
	@FXML
	public void searchFile(){
		fileSearch = new ArrayList<File>();
		itemsSearch = searchList.getItems();
		itemsSearch.clear();
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				SearchFile search = new SearchFile();
				if( driveBox.getValue() != null ){
					search.search(searchField.getText(), new File(driveBox.getValue()));
					List<File> files = search.getFileList();
					if (files != null) {
						for(File file : files){
							itemsSearch.add( file.getName() );
							fileSearch.add( file );
						}
					}
				}
				return null;
			}
			
			@Override
			protected void done() {}
			
		};
		worker.execute();
		currentFileList = fileSearch;
		currentItemList = itemsSearch;
	}

	/**
	 * A method that listens for a play button. Once clicked, 
	 * the program will play the selected audio file.
	 */
	@FXML
	public void play(){
		if(mediaplayer!=null){
			statusLabel.setText("Playing");
			if( playingIndex >= currentItemList.size() - 1 ) play( currentFileList.get(playingIndex).toURI().toASCIIString(), playingIndex);
			else {
				mediaplayer.play();
			}
			mediaplayer.setOnPlaying( new Runnable() {
				@Override
				public void run() {
					updateValues();
				}
			});
		}

	}

	/**
	 * A method that listens for a pause button. Once clicked
	 * the program will pause the playing audio file.
	 */
	@FXML
	public void pause(){
		if(mediaplayer!=null){
			statusLabel.setText("Pause");
			mediaplayer.pause();
		}
	}

	/**
	 * A method that listens for a shuffle button. Once clicked,
	 * the program will change playing state to shuffle and will
	 * play a random audio file upon the previous playing audio file
	 * endeds.
	 */
	@FXML
	public void shuffle(){
		if(ShuffleOn) this.ShuffleOn = false;
		else{
			this.ShuffleOn = true;
			this.RepeatOn = false;
			this.RepeatAllOn = false;
		}
	}

	/**
	 * A method that listens for a savePlaylist button. Once clicked,
	 * the program will output an object file which contains data
	 * of the current playlist which allow the output playlist to be
	 * put in next time.
	 */
	@FXML
	public void savePlaylist() {
		if( currentFileList != null ) {
			PlaylistIO pio = new PlaylistIO();
			for( File file: currentFileList ) pio.addFile( file );
			String tabName = tabPane.getSelectionModel().getSelectedItem().getText();
			DirectoryChooser directoryChooser = new DirectoryChooser();
			File directory = directoryChooser.showDialog( null );
			if( directory != null ){
				pio.writePlaylist( tabName, directory );
			}
		}
	}

	/**
	 * A method that listens for a stop button. Once clicked,
	 * the program will stop the playing audio file.
	 */
	@FXML
	public void stop(){
		if(mediaplayer!=null){
			statusLabel.setText("Stop");
			mediaplayer.stop();
		}
	}

	/**
	 * A method that listens for a next button. Once clicked,
	 * the program will stop the previous audio file and 
	 * play the next audio file in the selected playlist.
	 */
	@FXML
	public void next(){
		if(mediaplayer!=null){
			if(playingIndex == currentFileList.size()-1) playingIndex = -1;
			mediaplayer.stop();
			this.play(currentFileList.get( playingIndex+1 ).toURI().toASCIIString(), playingIndex+1);
		}
	}

	/**
	 * A method that listens for a previous button. Once clicked,
	 * the program will stop the previous audio file and play
	 * the audio file before the last audio file.
	 */
	@FXML
	public void previous(){
		if(mediaplayer!=null){
			if(playingIndex == 0) playingIndex += 1;
			mediaplayer.stop();
			this.play(currentFileList.get( playingIndex-1 ).toURI().toASCIIString(), playingIndex-1);
		}
	}

	/**
	 * A method that listens for a repeat button. Once clicked,
	 * the program will play the currently playing audio file
	 * continuously.
	 */
	@FXML
	public void repeat(){
		if(RepeatOn) this.RepeatOn = false;
		else{
			this.RepeatOn = true;
			this.ShuffleOn = false;
			this.RepeatAllOn = false;
		}
	}

	/**
	 * A method that listens for a repeatAll button. Once clicked,
	 * the program will play the hold playlist and start again 
	 * continuously.
	 */
	@FXML
	public void repeatAll(){
		if(RepeatAllOn) this.RepeatAllOn = false;
		else{
			this.RepeatAllOn = true;
			this.ShuffleOn = false;
			this.RepeatOn = false;
		}
	}

	/**
	 * A method that initialize components of a tab and add it
	 * to the displaying tabPane.
	 * @param tabPane - a tabPane to add new tab in
	 * @param tabTitle = the tab's title
	 */
	protected void createNewTab( TabPane tabPane, String tabTitle) {
		Tab tab = new Tab( tabTitle );
		AnchorPane tabContent = new AnchorPane();
		ListView<String> newListview = new ListView<String>();
		ArrayList<File> newFileList = new ArrayList<File>();
		ObservableList<String> newItemList = newListview.getItems(); 
		newListview.setPrefSize(660, 286);
		tabContent.setPrefSize(660, 286);
		tabContent.getChildren().add( newListview );
		tab.setContent( tabContent );
		tab.setClosable( true );
		final ObservableList<Tab> tabs = tabPane.getTabs();
		tabs.add( tabs.size() - 2, tab );
		storeListView.add( newListview );
		storefileList.add( newFileList );
		storeItemsList.add( newItemList );
		tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
		tab.setOnClosed(new EventHandler<Event>(){
			public void handle(Event event) {
				playlistCount--;
				storeListView.remove(currentTabIndex);
				storefileList.remove(currentTabIndex);
				storeItemsList.remove(currentTabIndex);
			}
		});
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		pane.setStyle("-fx-background: #BFEFFF;");
		mainPanel.setStyle("-fx-background: #00B2EE;");
		HBox.setHgrow( timeSlider, Priority.ALWAYS );
		timeSlider.setMinWidth( 50 );
		timeSlider.setMaxWidth( Double.MAX_VALUE );
		playTime.setPrefWidth( 130 );
		playTime.setMinWidth( 50 );
		
		filelist = new ArrayList<File>();
		items = listview.getItems();
		pb = new ProgressBar(0);
		pb.setMinWidth(222);
		pb.setMaxWidth(222);
		storefileList.add( filelist );
		storeListView.add( listview );
		storeItemsList.add( items );
		currentListView = listview;
		currentFileList = filelist;
		currentItemList = items;
		
		
		drive = FXCollections.observableArrayList(
				"C://",
				"D://"
				);
		driveBox.setItems(drive);

		volumeSlider.setPrefWidth(100);
		volumeSlider.setMaxWidth(Region.USE_PREF_SIZE);
		volumeSlider.setMinWidth(30);
		volumeSlider.valueProperty().addListener(new InvalidationListener() {
			public void invalidated(Observable ov) {
				if (volumeSlider.isValueChanging()) {
					if(mediaplayer != null){
						mediaplayer.setVolume( volumeSlider.getValue() / 100.0);
					}
				}
			}
		});

		tabPane.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> observable,
					Tab oldTab, Tab newTab) {
				if( newTab == addTab ){
					createNewTab(tabPane, "Playlist#"+(++playlistCount));
				}

				else if( newTab == searchTab){
					currentListView = searchList;
					currentFileList = fileSearch;
					currentItemList = itemsSearch;
					setCurrentTabProperty();
				}

				else {
					tabPane.getSelectionModel().select( newTab );
					currentTabIndex = tabPane.getSelectionModel().getSelectedIndex();
					currentListView = storeListView.get( currentTabIndex );
					currentFileList = storefileList.get( currentTabIndex );
					currentItemList = storeItemsList.get( currentTabIndex );
					setCurrentTabProperty();
				}
			}
		});

		setCurrentTabProperty();

	}

	/**
	 * A method that set the properties for currently selected
	 * tab.
	 */
	public void setCurrentTabProperty() {
		currentListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent click) {
				if ( click.getClickCount() == 2 && click.getButton().equals(MouseButton.PRIMARY) ) {
					if( currentItemList.size() == 0 ) return;
					if( mediaplayer != null ){
						mediaplayer.stop();
					}
					int selectedIndex = currentListView.getSelectionModel().getSelectedIndex();
					play( currentFileList.get( selectedIndex ).toURI().toASCIIString(), selectedIndex );

					timeSlider.valueProperty().addListener( new InvalidationListener() {

						@Override
						public void invalidated(javafx.beans.Observable ov) {
							if( timeSlider.isValueChanging() ) mediaplayer.seek( duration.multiply( timeSlider.getValue() / 100 ));
						}
					});
				}
			}
		});

		currentListView.setOnDragOver( dragEvent -> {
			if (dragEvent.getGestureSource() != currentListView &&
					dragEvent.getDragboard().hasFiles() ) {
				dragEvent.acceptTransferModes(TransferMode.MOVE);
			}
			dragEvent.consume();
		});

		currentListView.setOnDragDropped( dragEvent -> {
			Dragboard db = dragEvent.getDragboard();
			boolean success = false;
			if( db.hasFiles() ) {
				success = true;
				for( File file: db.getFiles() ) {
					String actualPath = file.toURI().toASCIIString();
					if( actualPath.endsWith( ".pl4" ) ) {
						PlaylistIO pio = PlaylistIO.readPlaylist( file );
						for( File audioFile: pio.getAudioFiles() ) {
							currentItemList.add( audioFile.getName() );
							currentFileList.add( audioFile );
						}
					}
					else {
						currentItemList.add( file.getName() );
						currentFileList.add( file );
					}
				}
			}

			dragEvent.setDropCompleted( success );

			dragEvent.consume();
		});
	}

	/**
	 * Play a song one by one until it's interrupted by another state.
	 * @param path = audio file's path 
	 * @param selectedIndex - index of the file in current playlist
	 */
	public void play( String path, int selectedIndex ) {
		if( mediaplayer != null ) prevVolume = mediaplayer.getVolume();
		playingIndex = selectedIndex;
		Media media = new Media( path );
		mediaplayer = new MediaPlayer( media );
		mediaplayer.setVolume( prevVolume );
		mediaplayer.play();
		statusLabel.setText("Playing");
		playingSong.setText( currentItemList.get( selectedIndex ).split("\\.")[0]  );
		mediaplayer.currentTimeProperty().addListener( new InvalidationListener() {
			@Override
			public void invalidated( javafx.beans.Observable arg0 ) {
				updateValues();
			}
		} );
		mediaplayer.setOnReady( new Runnable() {
			@Override
			public void run() {
				duration = mediaplayer.getMedia().getDuration();
			}
		} );
		mediaplayer.setOnEndOfMedia( new Runnable() {
			@Override
			public void run() {

				if( ShuffleOn ){
					playingIndex = (int) Math.floor(Math.random()*currentFileList.size());
				}
				else if( RepeatOn ){}
				else if( RepeatAllOn ) {
					if( playingIndex == currentFileList.size() - 1 ) playingIndex = 0;
					else playingIndex = selectedIndex + 1;;
				}
				else playingIndex = selectedIndex + 1;

				if( playingIndex >= currentFileList.size() ) {
					statusLabel.setText("Stop");
					mediaplayer.stop();
					playingIndex--;
				}
				else play( currentFileList.get( playingIndex ).toURI().toASCIIString(), playingIndex );
			}
		});
	}
	
	private void updateValues() {
		if( playTime != null && timeSlider != null && volumeSlider != null) {
			Platform.runLater( new Runnable() {
				@Override
				public void run() {
					Duration currentTime = mediaplayer.getCurrentTime();
					playTime.setText( formatTime( currentTime, duration ));
					timeSlider.setDisable( duration.isUnknown() );
					if( ! timeSlider.isDisabled() && duration.greaterThan( Duration.ZERO ) && !timeSlider.isValueChanging() ) {
						timeSlider.setValue( currentTime.divide( duration ).toMillis() * 100.0  )  ;
					}
					if (!volumeSlider.isValueChanging()) {
						volumeSlider.setValue((int)Math.round( mediaplayer.getVolume() * 100 ));
					}
				}
			} );
		}
	}

	private static String formatTime( Duration elapsed, Duration duration ) {
		int intElapsed = (int) Math.floor( elapsed.toSeconds() );
		int elapsedHours = intElapsed / (60 * 60);
		if (elapsedHours > 0) {
			intElapsed -= elapsedHours * 60 * 60;
		}
		int elapsedMinutes = intElapsed / 60;
		int elapsedSeconds = intElapsed - elapsedHours * 60 * 60 
				- elapsedMinutes * 60;

		if (duration.greaterThan(Duration.ZERO)) {
			int intDuration = (int)Math.floor(duration.toSeconds());
			int durationHours = intDuration / (60 * 60);
			if (durationHours > 0) {
				intDuration -= durationHours * 60 * 60;
			}
			int durationMinutes = intDuration / 60;
			int durationSeconds = intDuration - durationHours * 60 * 60 - 
					durationMinutes * 60;
			if (durationHours > 0) {
				return String.format("%d:%02d:%02d/%d:%02d:%02d", 
						elapsedHours, elapsedMinutes, elapsedSeconds,
						durationHours, durationMinutes, durationSeconds);
			} else {
				return String.format("%02d:%02d/%02d:%02d",
						elapsedMinutes, elapsedSeconds,durationMinutes, 
						durationSeconds);
			}
		} else {
			if (elapsedHours > 0) {
				return String.format("%d:%02d:%02d", elapsedHours, 
						elapsedMinutes, elapsedSeconds);
			} else 
				return String.format("%02d:%02d",elapsedMinutes, 
						elapsedSeconds);
		}
	}
}