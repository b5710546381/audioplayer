# AudioPlayer #

## Team member: ##

Pakpon Jetapai 5710546381

Phasin Sarunpornkul 5710546356

## Description: ##

An audio player program that support many audio file format (i.e. .mp3, .wav, etc.). The program
also allows users to make their own playlists.

## Feature: ##

 Main: 

  1. Play supported audio files 

  2. User can interact with the audio file that is playing (i.e. stop, pause, click the song timeline, etc.).

  3. Set repetition ( repeat once, repeat forever, repeat playlist, etc. ).
  
  4. Can make a playlist

 Extra:

  1. User can tune the current audio

  2. Show the cover of an album